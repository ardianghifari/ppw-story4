from django.conf.urls import url, include
from django.urls import path
from .views import *

urlpatterns = [
	
 	path(r'^home/', view_home, name = 'home'),
 	path(r'^feedback/', view_feedback, name = 'feedback'),
 	path(r'^register/', view_register, name = 'register'),
 	path(r'^thankyou/', view_thankyou, name = 'thankyou'),
 	path('', view_home, name = 'home2'),
]
