from django.shortcuts import render

# Create your views here.

def view_home(request):
	return render(request, 'Ardian Ghifari.html')

def view_feedback(request):
	return render(request, 'Feedback.html')

def view_register(request):
	return render(request, 'Register.html')

def view_thankyou(request):
	return render(request, 'Thank You.html')